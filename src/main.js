// Polyfills
import 'babel-polyfill';
import 'whatwg-fetch';
// ---------

import Vue from 'vue'
import App from './containers/App.vue'
import router from './router';
import store from './store';
import { initialize } from './actions';

Vue.config.productionTip = false;

store.dispatch(initialize).then(() => {
  new Vue({
    render: h => h(App),
    router,
    store
  }).$mount('#root');
});
