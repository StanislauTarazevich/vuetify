import qs from 'qs';
import { SPOTIFY_UNAUTHORIZED_ERROR } from "../settings";

const sendRequest = async (url, accessToken, data = {}) => {
  const headers = new Headers({
    'Authorization': `Bearer ${accessToken}`
  });

  const options = {
    method: 'GET',
    headers
  };

  let requestUrl = url;
  const queryString = qs.stringify(data);
  if (queryString) {
    requestUrl += `?${queryString}`;
  }

  const response = await fetch(requestUrl, options);

  if (response.status === 401) {
    throw new Error(SPOTIFY_UNAUTHORIZED_ERROR);
  }

  try {
    return await response.json();
  } catch (e) {
    return null;
  }
};

const parseTracks = (responseJson) => {
  const { total, next } = responseJson.tracks;
  const items = responseJson.tracks.items.map((item) => ({
    id: item.id,
    name: item.name,
    preview: item.preview_url,
    artistName: item.artists[0].name,
    albumName: item.album.name,
    albumImage: item.album.images[0] ? item.album.images[0].url : 'http://placehold.it/600x600'
  }));

  return {
    total,
    items,
    next
  }
};

const parseCurrentPlayback = (responseJson) => {
  const { item, device } = responseJson;
  return {
    id: item.id,
    isMobile: device.type === 'Smartphone',
    name: item.name,
    preview: item.preview_url,
    artistName: item.artists[0].name,
    albumName: item.album.name,
    albumImage: item.album.images[0] ? item.album.images[0].url : 'http://placehold.it/600x600'
  };
};

const parsePlaylists = (responseJson) => {
  const { total, next } = responseJson;
  const items = responseJson.items.map((item) => ({
    id: item.id,
    name: item.name,
    image: item.images[0] ? item.images[0].url: 'http://placehold.it/600x600',
    tracksCount: item.tracks.total,
    tracksUrl: item.tracks.href
  }));

  return {
    total,
    items,
    next
  }
};

const parsePlaylistTracks = (responseJson) => {
  const { total, next } = responseJson;
  const items = responseJson.items.map((item) => ({
    id: item.track.id,
    name: item.track.name,
    preview: item.track.preview_url,
    artistName: item.track.artists[0].name,
    albumName: item.track.album.name,
    albumImage: item.track.album.images[0] ? item.track.album.images[0].url : 'http://placehold.it/600x600'
  }));

  return {
    total,
    next,
    items
  }
};

export const me = async (accessToken) => {
  return await sendRequest('https://api.spotify.com/v1/me', accessToken);
};

export const searchTracks = async (query, accessToken) => {
  const responseJson = await sendRequest('https://api.spotify.com/v1/search', accessToken, {
    q: query,
    type: 'track',
    limit: 30
  });

  return parseTracks(responseJson);
};

export const searchMoreTracks = async (url, accessToken) => {
  const responseJson = await sendRequest(url, accessToken);
  return parseTracks(responseJson);
};

export const getCurrentPlayback = async (accessToken) => {
  const responseJson = await sendRequest('https://api.spotify.com/v1/me/player', accessToken);

  return responseJson ? parseCurrentPlayback(responseJson) : null;
};

export const getPlaylists = async (accessToken) => {
  const response = await sendRequest('https://api.spotify.com/v1/me/playlists', accessToken);
  return parsePlaylists(response);
};

export const getMorePlaylists = async (url, accessToken) => {
  const response = await sendRequest(url, accessToken);
  return parsePlaylists(response);
};

export const getPlaylistTracks = async (url, accessToken) => {
  const response = await sendRequest(url, accessToken);
  return parsePlaylistTracks(response);
};