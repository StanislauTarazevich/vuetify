export const initialize = 'INITIALIZE';
export const logout = 'LOGOUT';
export const retrieveTokens = 'RETRIEVE_TOKENS';
export const refreshToken = 'REFRESH_TOKEN';

export const submitSearch = 'SUBMIT_SEARCH';
export const showMoreSearchResults = 'SHOW_MORE_SEARCH_RESULTS';

export const listenPreview = 'LISTEN_PREVIEW';
export const dismissPlayer = 'DISMISS_PLAYER';

export const getPlaylists = 'GET_PLAYLISTS';
export const getMorePlaylists = 'GET_MORE_PLAYLISTS';
export const searchPlaylists = 'SEARCH_PLAYLISTS';
