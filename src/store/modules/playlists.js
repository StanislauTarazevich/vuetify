import { getPlaylists, getMorePlaylists, searchPlaylists } from '../../actions';
import { setPlaylists, addPlaylists, setPlaylistsQuery } from '../mutations';
import {
  getPlaylists as getPlaylistsApi,
  getMorePlaylists as getMorePlaylistsApi
} from '../../services/spotify';

export default {
  state: {
    query: '',
    next: null,
    total: null,
    list: []
  },

  getters: {
    playlistsQuery({ query }) {
      return query;
    },

    playlists({ list }) {
      return list;
    },

    filteredPlaylists({ query }, { playlists }) {
      return playlists.filter((playlist) => playlist.name.toLowerCase().includes(query.toLowerCase()))
    },

    playlistsTotal({ total }) {
      return total;
    },

    playlistsNext({ next }) {
      return next;
    }
  },

  mutations: {
    [setPlaylistsQuery](state, query) {
      state.query = query;
    },

    [setPlaylists](state, { items, next, total }) {
      state.list.splice(0, state.list.length);
      state.list.push(...items);
      state.next = next;
      state.total = total;
    },

    [addPlaylists](state, { items, next, total }) {
      state.list.push(...items);
      state.next = next;
      state.total = total;
    }
  },

  actions: {
    async [getPlaylists]({ commit, rootState }) {
      const accessToken = rootState.authentication.accessToken;
      const playlists = await getPlaylistsApi(accessToken);
      commit(setPlaylists, playlists);
    },

    async [getMorePlaylists]({ commit, rootState }, url) {
      const accessToken = rootState.authentication.accessToken;
      const playlists = await getMorePlaylistsApi(url, accessToken);
      commit(addPlaylists, playlists);
    },

    [searchPlaylists]({ commit }, query) {
      commit(setPlaylistsQuery, query);
    }
  }
};
