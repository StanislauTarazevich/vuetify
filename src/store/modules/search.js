import {showMoreSearchResults, submitSearch} from '../../actions';
import { setSearchQuery, setSearchResults, addSearchResults } from '../mutations';
import { searchTracks, searchMoreTracks } from '../../services/spotify';

export default {
  state: {
    query: '',
    next: null,
    total: null,
    results: []
  },

  getters: {
    searchQuery({ query }) {
      return query;
    },

    searchResults({ results }) {
      return results;
    },

    searchTotal({ total }) {
      return total;
    },

    searchNext({ next }) {
      return next;
    }
  },

  mutations: {
    [setSearchQuery](state, query) {
      state.query = query;
    },

    [setSearchResults](state, { items, next, total }) {
      state.results.splice(0, state.results.length);
      state.results.push(...items);
      state.next = next;
      state.total = total;
    },

    [addSearchResults](state, { items, next, total }) {
      state.results.push(...items);
      state.next = next;
      state.total = total;
    }
  },

  actions: {
    async [submitSearch]({ commit, rootState }, query) {
      commit(setSearchQuery, query);
      const accessToken = rootState.authentication.accessToken;
      const searchResponse = await searchTracks(query, accessToken);
      commit(setSearchResults, searchResponse);
    },

    async [showMoreSearchResults]({ commit, rootState }, url) {
      const accessToken = rootState.authentication.accessToken;
      const searchResponse = await searchMoreTracks(url, accessToken);
      commit(addSearchResults, searchResponse);
    }
  }
};
