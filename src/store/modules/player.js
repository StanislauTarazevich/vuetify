import { listenPreview, dismissPlayer } from '../../actions';
import { setPlayer, resetPlayer } from '../mutations';

export default {
  state: {
    track: null
  },

  getters: {
    playerTrack({ track }) {
      return track;
    }
  },

  mutations: {
    [setPlayer](state, track) {
      state.track = track;
    },

    [resetPlayer](state) {
      state.track = null
    }
  },

  actions: {
    async [listenPreview]({ commit }, track) {
      commit(setPlayer, track);
    },

    async [dismissPlayer]({ commit }) {
      commit(resetPlayer);
    }
  }
};
