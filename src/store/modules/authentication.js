import { initialize, logout, refreshToken, retrieveTokens } from '../../actions';
import { setTokens, removeTokens } from '../mutations';
import { me } from '../../services/spotify';
import { getTokens, getRefreshedToken } from '../../services/server';

const initialAccessToken = localStorage.getItem('accessToken');
const initialRefreshToken = localStorage.getItem('refreshToken');

export default {
  state: {
    accessToken: initialAccessToken,
    refreshToken: initialRefreshToken,
    isLoggedIn: false
  },

  getters: {
    accessToken({ accessToken }) {
      return accessToken;
    },

    refreshToken({ refreshToken }) {
      return refreshToken;
    },

    isLoggedIn({ isLoggedIn }) {
      return isLoggedIn;
    }
  },

  mutations: {
    [setTokens](state, { accessToken, refreshToken }) {
      state.accessToken = accessToken;
      state.refreshToken = refreshToken;
      state.isLoggedIn = true;
    },

    [removeTokens](state) {
      state.accessToken = '';
      state.refreshToken = '';
      state.isLoggedIn = false;
    }
  },

  actions: {
    async [retrieveTokens]({ commit }, code) {
      const {
        accessToken,
        refreshToken
      } = await getTokens(code);

      commit(setTokens, {
        accessToken,
        refreshToken
      });
    },

    async [refreshToken]({ commit, state }) {
      const refreshToken = state.refreshToken;
      if (refreshToken) {
        const accessToken = await getRefreshedToken(refreshToken);
        commit(setTokens, {
          accessToken,
          refreshToken
        });
      }
    },

    async [initialize]({ commit, state }) {
      const accessToken = state.accessToken;
      const refreshToken = state.refreshToken;

      let isLoggedIn = false;

      if (accessToken) {
        try {
          await me(accessToken);
          isLoggedIn = true;
        } catch (e) {
          isLoggedIn = false;
        }

        if (isLoggedIn) {
          commit(setTokens, {
            accessToken,
            refreshToken
          });
        }
      }

      if (!isLoggedIn && refreshToken) {
        const accessToken = await getRefreshedToken(refreshToken);
        commit(setTokens, {
          accessToken,
          refreshToken
        });
      }
    },

    [logout]({ commit }) {
      commit(removeTokens);
    }
  }
};
