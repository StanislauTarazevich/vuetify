export const setTokens = 'SET_TOKENS';
export const removeTokens = 'REMOVE_TOKENS';

export const setSearchQuery = 'SET_SEARCH_QUERY';
export const setSearchResults = 'SET_SEARCH_RESULTS';
export const addSearchResults = 'ADD_SEARCH_RESULTS';

export const setPlayer = 'SET_PLAYER';
export const resetPlayer = 'RESET_PLAYER';

export const setPlaylistsQuery = 'SET_PLAYLISTS_QUERY';
export const setPlaylists = 'SET_PLAYLISTS';
export const addPlaylists = 'ADD_PLAYLISTS';