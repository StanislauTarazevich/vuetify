import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

import authentication from './modules/authentication';
import player from './modules/player';
import playlists from './modules/playlists';
import search from './modules/search';
import localStorageTokensPlugin from './plugins/local-storage-tokens';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    authentication,
    player,
    playlists,
    search
  },

  plugins: [
    localStorageTokensPlugin,
    createLogger()
  ]
});