import { setTokens, removeTokens } from '../mutations';

export default store => {
  store.subscribe((mutation) => {
    if (mutation.type === setTokens) {
      const {
        accessToken,
        refreshToken
      } = mutation.payload;

      localStorage.setItem('accessToken', accessToken);
      localStorage.setItem('refreshToken', refreshToken);
    } else if (mutation.type === removeTokens) {
      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
    }
  })
}