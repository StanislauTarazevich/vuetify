import Vue from 'vue';
import VueRouter from 'vue-router';

import store from './store';

import Home from './components/Home';
import LogIn from './components/LogIn';
import LogInComplete from './containers/LogInComplete';
import Playlists from './containers/Playlists';
import Search from './containers/Search';

Vue.use(VueRouter);

const routes = [{
  path: '/',
  name: 'home',
  component: Home,
  meta: {
    authorize: true
  }
}, {
  path: '/search',
  name: 'search',
  component: Search,
  meta: {
    authorize: true
  }
}, {
  path: '/playlists',
  name: 'playlists',
  component: Playlists,
  meta: {
    authorize: true
  }
}, {
  path: '/login',
  name: 'logIn',
  component: LogIn
}, {
  path: '/login/complete',
  name: 'logInComplete',
  component: LogInComplete
}];

const router = new VueRouter({
  routes,
  mode: 'history'
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.authorize);
  const isLoggedIn = store.getters.isLoggedIn;

  if (requiresAuth && !isLoggedIn) {
    next({ name: 'logIn' });
  } else {
    next();
  }
});

export default router;